<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| ROUTES VISIBLE TO ALL USERS
|--------------------------------------------------------------------------
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index'); //real index
Route::get('/about', 'HomeController@about');
Route::get('/registracija', 'HomeController@register');
Route::get('/predavaci', 'HomeController@predavaci');
Route::get('/contact', 'HomeController@contact');
Route::any('/contact-form', 'ContactController@contact');
//Route::post('/contact-form', 'ContactController@contact');
Route::get('/uspesna-prijava', 'HomeController@success');

/*
|--------------------------------------------------------------------------
| LOGGED USERS ROUTES
|--------------------------------------------------------------------------
*/
Route::middleware(['auth'])->group(function () {
    Route::get('/kursevi', 'CoursesController@kursevi');
    Route::get('/kurs/{id}', 'CoursesController@kurs');
    Route::get('/premijera', 'CoursesController@premijera');
});

Auth::routes();

/*
|--------------------------------------------------------------------------
| BLOGS
|--------------------------------------------------------------------------
*/
Route::get('/blogs', 'BlogsController@index');
Route::get('/blog/{title}', 'BlogsController@blog');

/*
|--------------------------------------------------------------------------
| ADMIN ROUTES
|--------------------------------------------------------------------------
*/
Route::middleware(['admin'])->group(function () {
    Route::get('admin', 'Admin\AdminController@index');
    Route::resource('admin/blogs', 'Admin\BlogsController');
    Route::resource('admin/courses', 'Admin\CoursesController');
    Route::resource('admin/roles', 'Admin\RolesController');
    Route::resource('admin/permissions', 'Admin\PermissionsController');
    Route::resource('admin/users', 'Admin\UsersController');
    Route::get('admin/csv-download', 'Admin\UsersController@csv_download');
    Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
});
