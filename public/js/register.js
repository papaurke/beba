// disable html5 validation (temporary in jQuery)
$('input, select, textarea').on("invalid", function(e) {
    e.preventDefault();
});


function formValidation() {

    // first name
    var reg = /^[a-zA-Z]+$/;
    var firstName = document.registerForm.first_name.value;
    var firstNameVal = false;

    if (firstName == "") {
        document.getElementById('error_firstName').innerHTML = "Molimo vas unesite svoje ime!";
        document.registerForm.first_name.focus();
    }

    else if (reg.test(firstName) != true) {
        document.getElementById('error_firstName').innerHTML = "Ime može sadržati samo karaktere!";
        document.registerForm.first_name.focus();
    } else {
        document.getElementById('error_firstName').innerHTML = "&#x2714";
        document.getElementById('error_firstName').style.color = "green";
        firstNameVal = true;

    }

    // last name
    var lastName = document.registerForm.last_name.value;
    var lastNameVal = false;

    if (lastName == "") {
        document.getElementById('error_lastName').innerHTML = "Molimo vas unesite svoje prezime!";
        document.registerForm.last_name.focus();
    }

    else if (reg.test(lastName) != true) {
        document.getElementById('error_lastName').innerHTML = "Prezime može sadržati samo karaktere!";
        document.registerForm.last_name.focus();
    } else {
        document.getElementById('error_lastName').innerHTML = "&#x2714";
        document.getElementById('error_lastName').style.color = "green";
        lastNameVal = true;
    }

    // email
    var email = document.registerForm.email.value;
    var emailVal = false;
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (email == "") {
        document.getElementById('error_email').innerHTML = "Molimo vas unesite email.";
        document.registerForm.email.focus();
    }

    else if (!email.match(mailformat)) {
        document.getElementById("error_email").innerHTML = "Molimo vas unesite pravilan format email-a!";
        document.registerForm.email.focus();
    } else {
        document.getElementById('error_email').innerHTML = "&#x2714";
        document.getElementById('error_email').style.color = "green";
        emailVal = true;
    }

    // children
    var children = document.registerForm.children.value;

    if (children == "0") {
            // document.getElementById('error_child').innerHTML = "Molimo vas odaberite odgovor.";
            // document.registerForm.children.focus();
    } else {
        document.getElementById('error_child').innerHTML = "&#x2714";
        document.getElementById('error_child').style.color = "green";
    }

    // number of children
    var childrenNum = document.registerForm.children_nr.value;
    var numReg = /^(?:[1-9]\d?|99)$/;

    if (childrenNum == "") {
        document.getElementById('error_childNumber').innerHTML = "Molimo vas unesite odgovor.";
        document.registerForm.children_nr.focus();
    }

    else if (!childrenNum.match(numReg)) {
        document.getElementById('error_childNumber').innerHTML = "Molimo vas odgovor može biti samo dvocifreni broj.";
        document.registerForm.children_nr.focus();

        if (parseInt(childrenNum) > 99) {
            console.log('ee');
            document.getElementById('error_childNumber').innerHTML = "Vama ne trebaju usluge bebinara, čestitamo!";
            document.registerForm.children_nr.focus();
            document.getElementById('error_childNumber').style.color = "red";
        }
    } else {
        document.getElementById('error_childNumber').innerHTML = "&#x2714";
        document.getElementById('error_childNumber').style.color = "green";
    }

    // gender
    var gender = document.registerForm.gender.value;

    if (gender == "") {
        document.getElementById('error_gender').innerHTML = "Molimo vas odaberite rezultat.";
        document.registerForm.gender.focus();
    }

    else if (gender == "female") {
        document.registerForm.pregnancy_date.required();
    } else {
        document.getElementById('error_gender').innerHTML = "&#x2714";
        document.getElementById('error_gender').style.color = "green";
    }

    // pregnant
    var pregnant = document.registerForm.pregnant.value;

    if (pregnant == "") {
        document.getElementById('error_pregnant').innerHTML = "Molimo vas odaberite rezultat.";
        document.registerForm.pregnant.focus();
    } else {
        document.getElementById('error_pregnant').innerHTML = "&#x2714";
        document.getElementById('error_pregnant').style.color = "green";
    }

    // pregnant date
    var pregnantDate = document.registerForm.pregnancy_date.value;

    if (pregnantDate == "") {
        document.getElementById('error_pregnantDate').innerHTML = "Molimo vas odaberite datum termina.";
        document.registerForm.pregnancy_date.focus();
    } else {
        document.getElementById('error_pregnantDate').innerHTML = "&#x2714";
        document.getElementById('error_pregnantDate').style.color = "green";
    }

    // phone
    // var phone = document.registerForm.contact.value;
    //
    // if (pregnantDate == "") {
    //     document.getElementById('error_phone').innerHTML = "Molimo vas upišite broj telefona.";
    //     document.registerForm.contact.focus();
    // } else {
    //     document.getElementById('error_phone').innerHTML = "&#x2714";
    //     document.getElementById('error_phone').style.color = "green";
    // }

    // password
    var password = document.registerForm.password.value;
    var regPass = /[0-9a-zA-Z]{6,}/;

    if (password == "") {
        document.getElementById('error_pass').innerHTML = "Molimo vas unesite lozinku.";
        document.registerForm.password.focus();
    }

    else if (!password.match(regPass)) {
        document.getElementById("error_pass").innerHTML = "Molimo vas unesite lozinku sa najmanje 6 karaktera!";
        document.registerForm.password.focus();
    } else {
        document.getElementById('error_pass').innerHTML = "&#x2714";
        document.getElementById('error_pass').style.color = "green";
    }

    // password confirmation
    var confirmPass = document.registerForm.password_confirmation.value;

    if (confirmPass === "") {
        document.getElementById('error_cp').innerHTML = "Molimo vas potvrdite lozinku.";
        document.registerForm.password_confirmation.focus();
    }

    else if (confirmPass != password) {
        document.getElementById("error_cp").innerHTML = "Lozinke se ne poklapaju!";
        document.registerForm.password_confirmation.focus();
    } else {
        document.getElementById('error_cp').innerHTML = "&#x2714";
        document.getElementById('error_cp').style.color = "green";
    }
    // if(firstNameVal && lastNameVal && emailVal && confirmPass == password)
    //     document.getElementById("reserve").submit();
}


function resetFormFields() {
    document.getElementById('error_firstName').innerHTML = "";
    document.getElementById('error_lastName').innerHTML = "";
    document.getElementById('error_email').innerHTML = "";
    document.getElementById('error_child').innerHTML = "";
    document.getElementById('error_childNumber').innerHTML = "";
}

