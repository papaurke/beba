<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use App\Mail\RegisterUser;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     * Show the application home page.
     *
     */
    public function index()
    {
        $blogs = Blog::all();
        return view('pages.home')->with('blogs', $blogs);
    }

    /**
     *
     * Show the application about page.
     *
     */
    public function about()
    {
        return view('pages.about');
    }

    /**
     *
     * Show the application predavaci page.
     *
     */
    public function predavaci()
    {
        return view('pages.predavaci');
    }

    /**
     *
     * Show the application contact page.
     *
     */
    public function contact()
    {
        return view('pages.contact');
    }

    /**
     *
     * Show the application pisana rec page.
     *
     */
    public function pisanarec()
    {
        return view('pages.pisanarec');
    }

    /**
     *
     * Show the application register page.
     *
     */
    public function register()
    {
        return view('pages.register');
    }

    /**
     *
     * Show the application success page.
     *
     */
    public function success()
    {
        return view('pages.success');
    }


}
