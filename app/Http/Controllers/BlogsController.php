<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogsController extends Controller
{

    /**
     *
     * Get all blogs
     *
     */
    public function index()
    {
        return view('pages.blog')->with('data', Blog::all());
    }

    /**
     *
     * Get single blog by id
     *
     */
    public function blog($title)
    {
        $blog = Blog::where('title', $title)->first();
        $blog->title = str_replace('-',' ', $title);

        $blogs = Blog::all();

        return view('pages.blog')->with('blog', $blog)->with('blogs', $blogs);
    }

    /**
     * Handle a blog request, and createion
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function create(Request $request)
    {
        $blog = new Blog;

        $blog->title = str_replace(' ','-', $request->title);
        $blog->content = $request->content;
        $blog->user_id = $this->user()->id;

        $blog->save();

    }
    
}
