<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoursesController extends Controller
{
    /**
     *
     * Show the application kursevi page.
     *
     */
    public function kursevi()
    {
        $courses = Course::where('list', 1)
            ->where('premiere', 0)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('pages.kursevi')->with('data', $courses);
    }

    /**
     *
     * Show the application success page.
     *
     */
    public function premijera()
    {
        $course = Course::where('premiere', 1)->first();

        return view('pages.premijera')->with('data', $course);
    }

    /**
     *
     * Show the application success page.
     *
     */
    public function kurs($id)
    {
        if(Auth::user()->certificate != 1){
            $user = User::find(Auth::user()->id);
            $user->certificate = 1;
            $user->save();
        }

        return view('pages.kurs')->with('course', Course::findOrFail($id));
    }

}
