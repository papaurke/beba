<?php

namespace App\Http\Controllers\Auth;

use App\Mail\RegisterUser;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/uspesna-prijava';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'gender' => 'required|string',
        ];

        $messages = [
            'email.unique:users' => 'Vec postoji registrovan korisnik sa unesenom email adresom.',
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'children' => $data['children'],
            'children_nr' => isset($data['children_nr']) ? $data['children_nr'] : '0',
            'gender' => $data['gender'],
            'pregnant' => isset($data['pregnant']) ? $data['pregnant'] : 'no',
            'pregnancy_date' => isset($data['pregnancy_date']) ? $data['pregnancy_date'] : '2017-01-01',
            'country' => $data['country'],
            'city' => isset($data['city']) ? $data['city'] : 'no',
            'contact' => $data['contact'],
            'password' => bcrypt($data['password']),
        ]);

        Mail::send('emails.register', ['title' => 'Prijava', 'content' => 'Prijava', 'user' => $user, 'password' => $data['password']], function ($message) use ($user) {
            $message->from('prijava@bebinar.rs', 'Bebinar');
            $message->subject('Bebinar - Prijava');
            $message->to($user->email);
        });

        return $user;

    }

}
