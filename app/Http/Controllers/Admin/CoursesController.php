<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $courses = Course::where('title', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $courses = Course::paginate($perPage);
        }

        return view('admin.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $courses = Course::select('id', 'name', 'description', 'link', 'list', 'image', 'premiere')->get();

        return view('admin.courses.create', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required',
            'list' => 'required', 'image' => 'required', 'link' => 'required', 'premiere' => 'required']);

        $course = new Course;
        $course->name = $request->name;
        $course->description = $request->description;
        $course->list = $request->list;
        $course->image = $request->image;
        $course->link = $request->link;
        $course->premiere = $request->premiere;
        $course->save();

        return redirect('admin/courses')->with('flash_message', 'Course added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $course = Course::findOrFail($id);

        return view('admin.courses.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);

        return view('admin.courses.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required',
            'list' => 'required', 'image' => 'required', 'link' => 'required', 'premiere' => 'required']);

        $course = Course::findOrFail($id);
        $course->name = $request->name;
        $course->description = $request->description;
        $course->list = $request->list;
        $course->image = $request->image;
        $course->link = $request->link;
        $course->premiere = $request->premiere;
        $course->save();

        return redirect('admin/courses')->with('flash_message', 'Course updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Course::destroy($id);

        return redirect('admin/courses')->with('flash_message', 'Course deleted!');
    }
}
