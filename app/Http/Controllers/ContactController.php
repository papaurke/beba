<?php

namespace App\Http\Controllers;

use App\Http\Requests\Contacts;
use Illuminate\Support\Facades\Mail;
use App\Contact;

class ContactController extends Controller
{
    /**
     *
     * Save data from contact form.
     *
     */
    public function contact(Contacts $request)
    {
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->website = $request->website;
        $contact->content = $request->content;
        $contact->save();

        Mail::send('emails.contact', ['title' => 'Prijava', 'content' => $contact->content], function ($message) use ($contact) {
            $message->from($contact->email);
            $message->subject('Bebinar - Kontakt');
            $message->to('info@bebinar.com');
        });

        return 'Success';
    }

}
