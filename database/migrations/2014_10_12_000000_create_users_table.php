<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->enum('children', ['yes', 'no']);
            $table->string('children_nr')->nullable();
            $table->string('children_birth')->nullable();
            $table->string('email')->unique();
            $table->enum('gender', ['male', 'female']);
            $table->enum('pregnant', ['yes', 'no'])->nullable();
            $table->date('pregnancy_date')->nullable();
            $table->string('password');
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('contact')->nullable();
            $table->boolean('admin')->default(false);
            $table->boolean('certificate')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
