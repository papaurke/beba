Uspešno ste se registrovali na Bebinar <br> <br>

Vaš email za pristup: {{ $user->email }} <br>
Vaša Šifra: {{ $password }} <br> <br>

Uskoro očekujte email sa rasporedom predavanja <br> <br>

Srdačan pozdrav,<br>
Bebinar tim