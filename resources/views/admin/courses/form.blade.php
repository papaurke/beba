<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Naslov: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', 'Sadrzaj: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('link') ? ' has-error' : ''}}">
    {!! Form::label('link', 'Video Link: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('link', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
    {!! Form::label('image', 'Image Link: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('image', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('premiere') ? ' has-error' : ''}}">
    {!! Form::label('premiere', 'Premijera: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if(isset($course))
            {!! Form::select('premiere', array('0' => 'Ne', '1' => 'Da'), $course->premiere, ['class' => 'form-control', 'required' => 'required']); !!}
        @else
            {!! Form::select('premiere', array('0' => 'Ne', '1' => 'Da'), 0, ['class' => 'form-control', 'required' => 'required']); !!}
        @endif
        {!! $errors->first('premiere', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('list') ? ' has-error' : ''}}">
    {!! Form::label('list', 'Predavanja Lista: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if(isset($course))
            {!! Form::select('list', array('0' => 'Ne', '1' => 'Da'), $course->list, ['class' => 'form-control', 'required' => 'required']); !!}
        @else
            {!! Form::select('list', array('0' => 'Ne', '1' => 'Da'), 0, ['class' => 'form-control', 'required' => 'required']); !!}
        @endif
        {!! $errors->first('list', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
