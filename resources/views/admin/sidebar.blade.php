<div class="col-md-3">
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Admin
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('admin/users') }}">
                        Korisnici
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('admin/courses') }}">
                        Predavanja
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('admin/blogs') }}">
                        Blogovi
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
