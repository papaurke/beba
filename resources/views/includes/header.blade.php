<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112517807-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-112517807-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <title>Bebinar - Online škola roditeljstva</title>

    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="images/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="images/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="images/apple-touch-icon-152x152.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon-180x180.png"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('/rs-plugin/css/settings.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/fonts/font-awesome-4.3.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bbpress.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css') }}">

    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">

    <script src="{{ asset('/js/jquery.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            var date_input = $('input[name="pregnancy_date"]'); //our date input has the name "date"
            var container = $('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options = {
                format: 'yyyy-mm-dd',
                container: container,
                todayHighlight: true,
                autoclose: true,
                startDate: 'today',
                endDate: '+274d'
            };
            date_input.datepicker(options);
        });

    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


    <![endif]-->
    <link href="http://vjs.zencdn.net/6.6.0/video-js.css" rel="stylesheet">

    <!-- If you'd like to support IE8 -->
    <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>
<body>

<div id="loader">
    <div class="loader-container">
        <img src="{{ asset('/images/site.gif') }}" alt="" class="loader-site">
    </div>
</div>

<div id="wrapper">

    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <!-- <p><i class="fa fa-graduation-cap"></i> Best learning management template for ever.</p> -->
                </div><!-- end left -->

                <div class="col-md-6 text-right">
                    <ul class="list-inline">
                        <li>
                            <a class="social" href="https://www.facebook.com/bebinar.rs/"><i class="fa fa-facebook"></i></a>
                            <a class="social" href="https://twitter.com/Bebinar_rs"><i class="fa fa-twitter"></i></a>
                            <a class="social" href="https://www.instagram.com/bebinar.rs/"><i class="fa fa-instagram"></i></a>
                            <a class="social" href="https://www.youtube.com/channel/UClcjRaGyEYcO35g-aMRb89A"><i class="fa fa-youtube"></i></a>
                        </li>
                        @if (Auth::guest())
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-lock"></i>
                                    Logovanje & Registracija</a>
                                <div class="dropdown-menu">
                                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                                    <div class="form-title">
                                            <h4>Logovanje</h4>
                                            <hr>
                                        </div>
                                        <input class="form-control" type="text" name="email" placeholder="Email">
                                        <div class="formpassword">
                                            <input class="form-control" type="password" name="password"
                                                   placeholder="******">
                                        </div>
                                        <div class="clearfix"></div>
                                        <button type="submit" class="btn btn-block btn-primary">Uloguj se</button>
                                        <hr>

                                        <h4><a href="/password/reset">Resetuj password</a></h4>

                                        <hr>
                                        <h4><a href="/registracija">Kreiraj nalog</a></h4>
                                    </form>

                                    @else

                                        <a style="text-transform: lowercase" href="{{ url('/logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Izloguj se
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                    @endif
                                </div>
                            </li>
                    </ul>
                </div><!-- end right -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end topbar -->

    <header class="header">
        <div class="container">
            <div class="hovermenu ttmenu">
                <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="fa fa-bars"></span>
                        </button>
                        <div class="logo">
                            <a class="navbar-brand logo-pos" href="home"><img src="{{ asset('/images/logo2.png') }}" alt=""></a>
                        </div>
                    </div><!-- end navbar-header -->

                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            {{--<li class="dropdown ttmenu-half"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Početna <b class="fa fa-angle-down"></b></a>--}}
                            {{--<ul class="dropdown-menu menu-bg wbg">--}}
                            {{--<li>--}}
                            {{--<div class="ttmenu-content">--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="box">--}}
                            {{--<ul>--}}
                            {{--<li><a href="index1.html">Strana 1</a></li>--}}
                            {{--<li><a href="index2.html">Strana 2</a></li>--}}
                            {{--<li><a href="index3.html">Strana 3</a></li>--}}
                            {{--<li><a href="index4.html">Strana 4</a></li>--}}
                            {{--<li><a href="index5.html">Strana 5</a></li>--}}
                            {{--<li><a href="index6.html">Strana 6</a></li>--}}
                            {{--<li><a href="index7.html">Strana 7</a></li>--}}
                            {{--</ul>--}}
                            {{--</div><!-- end box -->--}}
                            {{--</div><!-- end col -->--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="box">--}}
                            {{--<ul>--}}
                            {{--<li><a href="index8.html">Strana 8</a></li>--}}
                            {{--<li><a href="index9.html">Strana 9</a></li>--}}
                            {{--<li><a href="index10.html">Strana 10</a></li>--}}
                            {{--</ul>--}}
                            {{--</div><!-- end box -->--}}
                            {{--</div><!-- end col -->--}}
                            {{--</div><!-- end row -->--}}
                            {{--</div><!-- end ttmenu-content -->--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</li><!-- end mega menu -->--}}
                            <li><a @if( Request::segment(1) == 'home' ) class="active" @endif href="/home">Home</a></li>
                            <li><a @if( Request::segment(1) == 'about' ) class="active" @endif href="/about">O Bebinaru</a></li>
                            <li><a @if( Request::segment(1) == 'kursevi' ) class="active" @endif href="/kursevi">Predavanja</a></li>
                            {{--<li><a @if( Request::segment(1) == '' )  class="active" @endif href="">Predavanja</a></li>--}}
                            <li><a @if( Request::segment(1) == 'premijera' )  class="active" @endif href="/premijera">Premijera</a></li>
                            <li><a @if( Request::segment(1) == 'predavaci' )  class="active" @endif href="/predavaci">Predavači</a></li>
                            <li><a @if( Request::segment(1) == 'blogs' )  class="active" @endif href="/blogs">Blog</a></li>
                            <li><a @if( Request::segment(1) == 'contact' ) class="active" @endif href="/contact">Kontakt</a></li>

                            {{--<li><a @if( Request::segment(1) == '' )  class="active" @endif href="">Predavači</a></li>
                            <li><a @if( Request::segment(1) == '' )  class="active" @endif href="">Intervju</a></li>--}}
                            {{--<li><a @if( Request::segment(1) == 'blogs' )  class="active" @endif href="blogs">Blog</a></li>--}}
                            {{--<li><a @if( Request::segment(1) == 'kursevi' ) class="active"
                                   @endif href="kursevi">Kursevi</a></li>--}}
                            {{--<li class="dropdown ttmenu-half"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Kursevi <b class="fa fa-angle-down"></b></a>--}}
                            {{--<ul class="dropdown-menu">--}}
                            {{--<li class="dropdown ttmenu-half"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Kursevi <b class="fa fa-angle-down"></b></a>--}}
                            {{--<li>--}}
                            {{--<div class="ttmenu-content">--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="box">--}}
                            {{--<ul>--}}
                            {{--<li><a href="">Lista kurseva</a></li>--}}
                            {{--<li><a href="">Pretraga kurseva</a></li>--}}
                            {{--<li><a href="">Poseban kurs</a></li>--}}
                            {{--<li><a href="">Arhiva</a></li>--}}
                            {{--</ul>--}}
                            {{--</div><!-- end box -->--}}
                            {{--</div><!-- end col -->--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="box">--}}
                            {{--<ul>--}}
                            {{--<li><a href="">Instruktori</a></li>--}}
                            {{--<li><a href="">Forum</a></li>--}}
                            {{--<li><a href="">Logovanje & Registracija</a></li>--}}
                            {{--<li><a href="">Edituj svoj nalog</a></li>--}}
                            {{--<li><a href="">Najcesca pitanja</a></li>--}}
                            {{--</ul>--}}
                            {{--</div><!-- end box -->--}}
                            {{--</div><!-- end col -->--}}
                            {{--</div><!-- end row -->--}}
                            {{--<hr>--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-3 col-sm-6 nopadding">--}}
                            {{--<img class="img-thumbnail" src="upload/service_01.png" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3 col-sm-6 nopadding">--}}
                            {{--<img class="img-thumbnail" src="upload/service_02.png" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3 col-sm-6 nopadding">--}}
                            {{--<img class="img-thumbnail" src="upload/service_03.png" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3 col-sm-6 nopadding">--}}
                            {{--<img class="img-thumbnail" src="upload/service_04.png" alt="">--}}
                            {{--</div>--}}
                            {{--</div><!-- end row -->--}}
                            {{--</div><!-- end ttmenu-content -->--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</li><!-- end mega menu -->--}}
                            {{--<li><a @if( Request::segment(1) == 'blogs' ) class="active" @endif href="blogs">Blog</a>
                            </li>--}}
                        </ul><!-- end nav navbar-nav -->
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="btn btn-primary" href="registracija"><i class="fa fa-sign-in"></i>Prijavi se</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!-- end navbar navbar-default clearfix -->
            </div><!-- end menu 1 -->
        </div><!-- end container -->
    </header><!-- end header -->