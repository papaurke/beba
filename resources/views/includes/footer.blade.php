<footer class="dark footer section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-md-9 col-xs-12">
                                <div class="widget">
                                    <div class="widget-title">
                                        <h4>O bebinaru</h4>
                                        <hr>
                                    </div>
                                    <p>Sadržaj na Bebinaru je informativnog karaktera i nikako ne predstavlja zamenu za informacije koje vam pruža vaš lekar ili lekar vašeg deteta.</p>
                                    <p>Odgovornost za sadržaj predavanja i prezentacija je isključivo na predavačima, koji su se obavezali da će sadržaj prezentovati sa pažnjom dobrog stručnjaka, savesno, efikasno i u skladu sa opšte prihvaćenim pravilima struke, etičkim kodeksom i profesionalnim ponašanjem kao i uobičajenom praksom za tu vrstu posla.</p>
                                    <p>Svi sadržaji na sajtu Bebinar.rs su potpuno besplatni ali podležu autorskim i drugim pravima. </p>
                                    {{--<a href="about" class="btn btn-default">Pročitaj više</a>--}}
                                </div><!-- end widget -->
                            </div><!-- end col -->

                            <div class="col-md-3 col-md-3 col-xs-12" style="text-align: center;">
                                <div class="widget">
                                    <div class="widget-title" style="text-align: left;">
                                        <h4>Kontakt</h4>
                                        <hr>
                                    </div>

                                    <ul class="contact-details">
                                        <li><i class="fa fa-link"></i> <a href="#">www.bebinar.rs</a></li>
                                        <li><i class="fa fa-envelope"></i> <a href="mailto:info@bebinar.rs">info@bebinar.rs</a></li>
                                        <li><i class="fa fa-phone"></i> ‎+381 64 18 87 462</li>
                                    </ul>

                                </div><!-- end widget -->
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end container -->
                </footer><!-- end section -->

                <section class="copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <p>© 2018 Sva prava zadržava Bebinar.</p>
                            </div><!-- end col -->
                            <div class="col-md-6 text-right">
                                <ul class="list-inline">
                                    {{--<li><a href="#">Terms of Usage</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Sitemap</a></li>--}}
                                </ul>
                            </div>
                        </div><!-- end row -->
                    </div><!-- end container -->
                </section><!-- end section -->


<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/retina.js') }}"></script>
<script src="{{ asset('/js/wow.js') }}"></script>
<script src="{{ asset('/js/carousel.js') }}"></script>
<script src="{{ asset('/js/parallax.min.js') }}"></script>
<!-- CUSTOM PLUGINS -->
<script src="{{ asset('/js/custom.js') }}"></script>
<!-- SLIDER REV -->
<script src="{{ asset('/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>


<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
    jQuery(document).ready(function() {
        jQuery('.tp-banner').show().revolution(
            {
                dottedOverlay:"none",
                delay:16000,
                startwidth:1170,
                startheight:820,
                hideThumbs:200,
                thumbWidth:100,
                thumbHeight:50,
                thumbAmount:5,
                navigationType:"none",
                navigationArrows:"solo",
                navigationStyle:"preview3",
                touchenabled:"on",
                onHoverStop:"on",
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
                parallax:"mouse",
                parallaxBgFreeze:"on",
                parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
                parallaxDisableOnMobile:"off",
                keyboardNavigation:"off",
                navigationHAlign:"center",
                navigationVAlign:"bottom",
                navigationHOffset:0,
                navigationVOffset:20,
                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,
                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,
                shadow:0,
                fullWidth:"on",
                fullScreen:"off",
                spinner:"spinner4",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                forceFullWidth:"off",
                hideThumbsOnMobile:"off",
                hideNavDelayOnMobile:1500,
                hideBulletsOnMobile:"off",
                hideArrowsOnMobile:"off",
                hideThumbsUnderResolution:0,
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                startWithSlide:0,
                fullScreenOffsetContainer: ""
            });
    });
</script>

<!-- CUSTOM PLUGINS -->
<script src="{{ asset('/js/custom.js') }}"></script>
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
<script src="{{ asset('/js/contact.js') }}"></script>
<script src="{{ asset('/js/register.js') }}"></script>
<script src="{{ asset('/js/map.js') }}"></script>

</body>
</html>