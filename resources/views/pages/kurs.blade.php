@extends('index')
@section('content')
    <section class="white section single-course">
        <div class="container">
            <video oncontextmenu="return false;" id="course-video" width="100%" height="auto" max-height="100%" autoplay controls controlsList="nodownload">
                <source src="{{ $course->link }}" type="video/mp4">
                Vaš browser ne podržava video tag!.
            </video>
        </div>
    </div>

@endsection