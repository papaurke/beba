@extends('index')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        @if (Auth::guest())
                            <form id="reserve" name="registerForm" class="form-horizontal" method="POST"
                                  action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name" class="col-md-3 control-label">Ime:</label>
                                    <div class="col-md-6">
                                        <input formnovalidate placeholder="Unesite ime" type="text" class="form-control"
                                               name="first_name" value="{{ old('first_name') }}" required="required"
                                               id="first_name" autofocus oninvalid="setCustomValidity('Popunite polje')"/>
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_firstName" class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name" class="col-md-3 control-label">Prezime:</label>
                                    <div class="col-md-6">
                                        <input placeholder="Unesite prezime" id="last_name" type="text" class="form-control"
                                               name="last_name" value="{{ old('name') }}" required="required">
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_lastName" class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-3 control-label">Email:</label>
                                    <div class="col-md-6">
                                        <input placeholder="Unesite email" id="email" type="email" class="form-control"
                                               name="email" value="{{ old('email') }}" required="required">
                                                @if ($errors->has('email'))
                                                    @if ($errors->first('email') == 'The email has already been taken.')
                                                        <div class="error">Vec postoji registrovan korisnik sa unesenom email adresom.</div>
                                                    @endif

                                                @endif
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_email" class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('children') ? ' has-error' : '' }}">
                                    <label for="children" class="col-md-3 control-label">Da li imate dece?</label>
                                    <div class="col-md-6">
                                        <select id="children" type="children" class="selectpicker-children form-control"
                                                name="children" value="" required>
                                            <option value="0"></option>
                                            <option value="yes">Da</option>
                                            <option value="no">Ne</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_child" class="error"></span>
                                    </div>
                                </div>

                                <div class="children-number form-group{{ $errors->has('children_nr') ? ' has-error' : '' }}" hidden>
                                    <label for="children_nr" class="col-md-3 control-label">Koliko dece imate?</label>
                                    <div class="col-md-6">
                                        <input id="children_nr" type="children_nr" class="form-control"
                                               name="children_nr" value="{{ old('children_nr') }}" />
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_childNumber" class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <label for="gender" class="col-md-3 control-label">Pol</label>
                                    <div class="col-md-6">
                                        <select id="gender" type="gender" class="selectpicker-pregnant form-control"
                                                name="gender" value="" required>
                                            <option disabled selected hidden="selected"></option>
                                            <option value="female">Ženski</option>
                                            <option value="male">Muški</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_gender" class="error"></span>
                                    </div>
                                </div>

                                <div class="pregnant form-group{{ $errors->has('pregnant') ? ' has-error' : '' }}"
                                     hidden>
                                    <label for="pregnant" class="col-md-3 control-label">Trudnoća</label>
                                    <div class="col-md-6">
                                        <select id="pregnant" type="pregnant" class="selectpicker-pregnancy form-control" name="pregnant"
                                                value="">
                                            <option disabled selected hidden="selected"></option>
                                            <option value="yes">Da</option>
                                            <option value="no">Ne</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_pregnant" class="error"></span>
                                    </div>
                                </div>

                                <div class="pregnancy form-group{{ $errors->has('pregnancy_date') ? ' has-error' : '' }}" hidden>
                                    <label for="pregnant_date" class="col-md-3 control-label">Predviđeni termin porođaja</label>
                                    <div class="col-md-6">
                                        <input class="pregnancy-picker form-control" id="date" name="pregnancy_date" placeholder="MM/DD/YYY" type="text" readonly="readonly"/>
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_pregnantDate" class="error"></span>
                                    </div>
                                </div>

                                <div class="selectpicker-country form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label for="country" class="col-md-3 control-label">Država</label>
                                    <div class="col-md-6">
                                        <select id="country" type="country" class="form-control" name="country"
                                                value="">
                                            <option selected="selected" value="Srbija">Srbija</option>
                                            <option value="Bosna i Hercegovina">Bosna i Hercegovina</option>
                                            <option value="Crna Gora">Crna Gora</option>
                                            <option value="Makedonija">Makedonija</option>
                                            <option value="Hrvatska">Hrvatska</option>
                                            <option value="Hrvatska">drugo</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                </div>

                                <div class="serbia-cities form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="col-md-3 control-label">Grad</label>
                                    <div class="col-md-6">
                                        <select id="city" type="city" class="form-control" name="city" value="">
                                            <option selected="selected" value="Beograd">Beograd</option>
                                            <option value="Novi Sad">Novi Sad</option>
                                            <option value="Priština">Priština</option>
                                            <option value="Niš">Niš</option>
                                            <option value="Kragujevac">Kragujevac</option>
                                            <option value="Subotica">Subotica</option>
                                            <option value="Pančevo">Pančevo</option>
                                            <option value="Zrenjanin">Zrenjanin</option>
                                            <option value="Čačak">Čačak</option>
                                            <option value="Kruševac">Kruševac</option>
                                            <option value="Kraljevo">Kraljevo</option>
                                            <option value="Novi Pazar">Novi Pazar</option>
                                            <option value="Leskovac">Leskovac</option>
                                            <option value="Smederevo">Smederevo</option>
                                            <option value="Vranje">Vranje</option>
                                            <option value="Užice">Užice</option>
                                            <option value="Valjevo">Valjevo</option>
                                            <option value="Šabac">Šabac</option>
                                            <option value="Požarevac">Požarevac</option>
                                            <option value="Sombor">Sombor</option>
                                            <option value="Pirot">Pirot</option>
                                            <option value="Zaječar">Zaječar</option>
                                            <option value="Kikinda">Kikinda</option>
                                            <option value="Sremska Mitrovica">Sremska Mitrovica</option>
                                            <option value="Jagodina">Jagodina</option>
                                            <option value="Vršac">Vršac</option>
                                            <option value="Loznica">Loznica</option>
                                        </select>
                                        <div class="col-md-3">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                                    <label for="contact" class="col-md-3 control-label">Telefon</label>
                                    <div class="col-md-6">
                                        <input id="contact" type="contact" placeholder="Unesite telefon" class="form-control" name="contact"
                                               value="{{ old('contact') }}">
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_phone" class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-3 control-label">Lozinka</label>
                                    <div class="col-md-6">
                                        <input placeholder="Unesi lozinku da ima više od 6 karaktera" id="password" type="password" class="form-control"
                                            name="password" required>
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_pass" class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password-confirm" class="col-md-3 control-label">Potvrdi lozinku</label>
                                    <div class="col-md-6">
                                        <input placeholder="Potvrdi lozinku" id="password-confirm" type="password"
                                               class="form-control" name="password_confirmation" required>
                                    </div>
                                    <div class="col-md-3 error-container">
                                        <span id="error_cp" class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group register-buttons">
                                    <div class="col-md-6 col-md-offset-3 col-sm-5 col-sm-offset-7 col-xs-10 col-xs-offset-2 eee">
                                        <button type="submit" class="btn btn-primary" id="register" onclick="formValidation()" />
                                            Registruj se
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @else
                            @include('pages.success')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
