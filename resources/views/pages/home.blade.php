@extends('index')
@section('content')

    <section class="slider-section">
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                        data-thumb="upload/slider_new_03.jpg" data-saveperformance="off" data-title="Slide">
                        <video oncontextmenu="return false;" width="100%" height="auto" max-height="100%" autoplay loop>
                            <source src="video/bebinar2000br.mp4" type="video/mp4">
                                Vaš browser ne podržava video tag!.
                        </video>
                        <div class="tp-caption text-center lft tp-resizeme"
                             data-x="center"
                             data-y="240"
                             data-speed="1000"
                             data-start="600"
                             data-easing="Power3.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                             <img src="images/logo2.png">
                        </div>
                        <div class="tp-caption slider_layer_01 text-center lft tp-resizeme"
                             data-x="center"
                             data-y="350"
                             data-speed="1000"
                             data-start="600"
                             data-easing="Power3.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">PRVA ONLINE ŠKOLA RODITELJSTVA U SRBIJI
                        </div>
                        <div class="tp-caption slider_layer_02 text-center lft tp-resizeme subtitle"
                             data-x="center"
                             data-y="460"
                             data-speed="1000"
                             data-start="800"
                             data-easing="Power3.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             style="z-index: 9;  color: white;"><p>Predavanja stručnjaka za Vašu bezbrižnu trudnoću i saveti za roditelje</p>
                        </div>
                        <div class="tp-caption text-center lft tp-resizeme"
                             data-x="center"
                             data-y="540"
                             data-speed="1000"
                             data-start="800"
                             data-easing="Power3.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                             <a href="{{ url('registracija') }}" class="btn btn-default">Prijavi se</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section class="white section why-bebinar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Zašto Bebinar?</h4>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="why-us text-right wow fadeInLeft left-icons" data-wow-duration="1s" data-wow-delay="0.3s">
                        <i class="fa alignright"><img class="icons" src="images/book.png"></img></i>
                        <h4>1. Saveti stručnjaka</h4>
                        <p>Obrađujemo najinteresantnije teme, kako bi Vaša trudnoća protekla bezbrižno. Profesionalne i proverene informacije su od sad dostupne svakoj trudnici.</p>
                    </div>
                    <div class="why-us text-right wow fadeInLeft left-icons" data-wow-duration="1s" data-wow-delay="0.7s">
                        <i class="fa alignright"><img class="icons" src="images/questonmark.png"></img></i>
                        <h4>2. Postavite pitanja</h4>
                        <p>Znanje i iskustvo stručnjaka dostupno baš svima, bez obzira na mesto stanovanja. Mogućnost interakcije sa predavačima.</p>
                    </div>
                    <div class="why-us text-right wow fadeInLeft left-icons" data-wow-duration="1s" data-wow-delay="1.1s">
                        <i class="fa alignright"><img class="icons" src="images/no-money.png"></img></i>
                        <h4>3. Sve je besplatno</h4>
                        <p>Bebinar je besplatan za sve buduće roditelje. Nastali smo zbog Vas.</p>
                    </div>
                </div><!-- end col -->
                <div class="col-md-6 col-sm-6">
                    <div class="why-us text-left wow fadeInRight right-icons" data-wow-duration="1s" data-wow-delay="0.5s">
                        <i class="fa alignleft"><img class="icons" src="images/digital.png"></img></i>
                        <h4>4. Savremena tehnologija</h4>
                        <p>Vaša deca će odrastati u digitalnom okruženju. Zar ovo nije idealan početak? :)</p>
                    </div>
                    <div class="why-us text-left wow fadeInRight right-icons" data-wow-duration="1s" data-wow-delay="0.9s">
                        <i class="fa alignleft"><img class="icons" src="images/devices.png"></img></i>
                        <h4>5. Mogućnost gledanja na svim uređajima</h4>
                        <p>Sadržaj je sa jednakim kvalitetom moguće gledati na računaru, tabletu ili telefonu</p>
                    </div>
                    <div class="why-us text-left wow fadeInRight right-icons" data-wow-duration="1s" data-wow-delay="1.3s">
                        <i class="fa fa-leaf alignleft"></i>
                        <h4>6. Apsolutni komfor</h4>
                        <p>Mislite da je savršeno vreme za gledanje predavanja sreda od 23h? Slažemo se - gledajte tada</p>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->


    <section class="section darkskin fullscreen paralbackground parallax"
            style="background-image:url('upload/three-steps.jpg');";
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            background-attachment: inherit;
            background-position: center;
            background-repeat: no-repeat;
            position: fixed;
            z-index: -1;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0; >

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Prijava na bebinar u tri koraka:</h4>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row service-center">
                <div class="col-md-4 col-sm-6">
                    <div class="feature-list">
                        <i>1</i>
                        <p><strong>Popunite vaše podatke na stranici Prijava</strong></p>
                    </div><!-- end service -->
                </div><!-- end col -->

                <div class="col-md-4 col-sm-6">
                    <div class="feature-list">
                        <i>2</i>
                        <p><strong>Na upisani email vam stigne potvrda registracije</strong></p>
                    </div><!-- end service -->
                </div><!-- end col -->

                <div class="col-md-4 col-sm-6">
                    <div class="feature-list">
                        <i>3</i>
                        <p><strong>Pristupate predavanjima</strong></p>
                    </div><!-- end service -->
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row button-wrapper">
                <div class="col-md-12">
                    <div class="text-center">
                        <a href="{{ url('registracija') }}" class="btn btn-default">Prijavi se</a>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->

    <section class="grey section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Sa bloga</h4>
                        <p>Podelićemo sa vama najbolje teme</p>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row blog-widget">
                @foreach ($blogs as $data)
                    <div class="col-md-4 col-sm-6">
                        <div class="blog-wrapper">
                            <div class="blog-title home-blog">
                                <a class="category_title" href="" title=""></a>
                                <h2><a class="blogTitle" href="/blog/{{ $data->title }}" title="">{{ $data->title }}</a></h2>

                            </div><!-- end blog-title -->
                            <div class="blog-image">
                                <a href="single.html" title=""><img src="{{ $data->image }}" alt="" class="img-responsive"></a>
                            </div><!-- end image -->
                            <div class="blog-desc">
                                <p class="content little-blogs">{!! $data->content !!}</p>
                                <a href="/blog/{{ $data->title }}" class="btn btn-default btn-block">Pročitaj više...</a>
                            </div><!-- end desc -->
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col -->

                @endforeach

            </div><!-- end row -->

        </div><!-- end container -->
    </section><!-- end section -->

    <script>
        var el = document.querySelectorAll('.content'),
            titleEl = document.querySelectorAll('.blogTitle'),
            textLength = 300,
            titleLength = 80,
            trimmedTitleEl,
            trimmedText,
            newTitleText,
            text,
            title,
            i;

        // trim function
        function smartTrim(str, length, delim, appendix) {
            if (str.length <= length) return str;

            var trimmedStr = str.substr(0, length + delim.length);

            var lastDelimIndex = trimmedStr.lastIndexOf(delim);
            if (lastDelimIndex >= 0) trimmedStr = trimmedStr.substr(0, lastDelimIndex);

            if (trimmedStr) trimmedStr += appendix;

            return trimmedStr;
        }


        for (i = 0; i < el.length; i++) {
            text = el[i].innerHTML
            trimmedText = smartTrim(text, textLength, ' ', ' ...');

            el[i].innerHTML = trimmedText;
        }

        for (i = 0; i < titleEl.length; i++) {
            title = titleEl[i].innerHTML
            trimmedTitleText = smartTrim(title, titleLength, ' ', ' ...');

            newTitleText = trimmedTitleText.replace(/[-\s]/g," ");
            titleEl[i].innerHTML = newTitleText;
        }
    </script>

@stop
