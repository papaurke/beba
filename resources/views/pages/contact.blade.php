@extends('index')
@section('content')

            <section class="white section bebinar-contact">
            <div class="row">
                <img src="upload/kontakt-bebinar.jpg" width="100%" height="auto"/>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h4>Kontaktirajte nas</h4>
                            <p>Kontaktirajte nas već danas i podelite informacije sa nama</p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row contact-wrapper">
                    <div class="col-md-9 col-sm-9 col-xs-12 content-widget">
                        <div class="widget-title">
                            <h4>Kontakt forma</h4>
                            <hr>
                        </div>
                        <div id="contact_form" class="contact_form row">
                            <div id="message"></div>
                            <form id="contactform" action="contact-form" name="contactform" method="post">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Ime *">
                                    <input type="text" name="email" id="email" class="form-control" placeholder="Email *">
                                    <input type="text" name="name" id="website" class="form-control" placeholder="Vebsajt">
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <textarea class="form-control" name="comments" id="comments" rows="6" placeholder=""></textarea>
                                    <button type="submit" value="SEND" id="submit" class="btn btn-primary btn-block">Pošalji poruku</button>
                                </div>
                            </form>
                        </div><!-- end contact-form -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-3 col-xs-12 content-widget">
                        <div class="widget-title">
                            <h4>Informacije</h4>
                            <hr>
                        </div>
                        <div class="contact-list">
                            <ul class="contact-details">
                                <li><i class="fa fa-link"></i> <a href="#">www.bebinar.rs</a></li>
                                <li><i class="fa fa-envelope"></i> <a href="mailto:info@bebinar.rs">info@bebinar.rs</a></li>
                                <li><i class="fa fa-phone"></i> ‎+381 64 18 87 462</li>
                            </ul>
                        </div><!-- end contact-list -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

@endsection