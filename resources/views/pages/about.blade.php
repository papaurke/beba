@extends('index')
@section('content')

    <section class="grey page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <h1 style="font-style: bold">O Nama</h1>
                </div><!-- end col -->
                {{--<div class="col-md-6 text-right">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Pages</a></li>
                            <li class="active">O nama</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- end col -->--}}    
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->

    <section class="white section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="media-element">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 30px;">
                            <div class="carousel-inner" role="listbox">
                                <img src="images/o-nama.jpg" class="image-responsive">
                            </div>
                        </div>
                    </div><!-- end blog-image -->
                </div><!-- end col -->

                <div class="col-md-6">
                    <div class="content-widget">
                        <div class="widget-title">
                            <h4>Bebinar</h4>
                            <hr>
                        </div>
                        <p>Bebinar je prva online škola roditeljstva u Srbiji i regionu.</p>

                            <p>Želja nam je da trudnicama, odnosno budućim roditeljima, ponudimo savete stručnjaka i profesionalaca iz čitavog spektra oblasti, koje mogu biti sfera interesovanja pomenutih društvenih grupa.</p>

                            <p>Tehnički, Bebinar je koncipiran tako da se sadržaj može konzumirati na računaru, telefonu ili tabletu, uz minimalno digitalno znanje.</p>

                            <p>Sva predavanja na Bebinaru su potpuno besplatna.</p>

                            <p>Škola traje jedan kalendarski mesec i u tom periodu je, odmah nakon premijernog emitovanja, sadržaj predavanja dostupan do poslednjeg dana u mesecu.</p>
                        </p>

                        <p class="attention">Prijava važi jednu kalendarsku godinu!</p>
                        <p>Bebinar je podržalo Ministarstvo trgovine, turizma i telekomunikacije Republike Srbije</p>
                        <img src="images/grb.png"/>

                    </div>
                </div><!-- end col -->
                <div class="col-md-6 col-sm-12 col-xs-12 iframe-column">
                    <div class="item active video-about-container">
                        <iframe class="video-about" width="100%" height="100%" src="https://www.youtube.com/embed/KA_azf-htjk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->

@endsection