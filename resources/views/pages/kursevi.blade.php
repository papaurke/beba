@extends('index')
@section('content')
    <section class="grey page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <h1 style="font-style: bold">Predavanja</h1>
                </div><!-- end col -->
                {{--<div class="col-md-6 text-right">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Courses</a></li>
                            <li class="active">Course List Page</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- end col -->--}}
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->

    <section class="white section">
        <div class="container">
        

            @foreach($data as $course)
                <div class="row course-list">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <a href="/kurs/{{ $course->id }}">
                            <div class="shop-item-list entry">
                                <div class="course-video-image">
                                    <img src="{{ $course->image }}" alt="">
                                    <div class="magnifier">
                                    </div>
                                </div>
                            </div><!-- end shop-item -->
                        </a>
                    </div><!-- end col -->

                    <div class="col-md-8 col-md-12 col-xs-12">
                        <div class="shop-list-desc">
                            <h4><a href="/kurs/{{ $course->id }}">{{ $course->name }}</a></h4>
                            <div class="shopmeta">
                                {{--<span class="pull-left"><strong>Course Price:</strong> $21.00 </span>--}}
                                {{--<div class="rating pull-right">--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--</div><!-- end rating -->--}}
                            </div><!-- end shop-meta -->

                            <hr class="invis clearfix">

                            <p>{{ $course->description }}</p>

                        </div><!-- end shop-list-desc -->
                    </div><!-- end col -->

                    <div class="col-md-10 col-md-12 col-xs-12">
                        <a href="/kurs/{{ $course->id }}" class="btn btn-default btn-sm">Video</a>
                    </div>
                </div><!-- end row -->
                <hr>
            @endforeach

        </div><!-- end container -->
    </section><!-- end section -->

        </div><!-- end container -->
    </section><!-- end section -->

@endsection