@extends('index')
@section('content')
    <section class="grey page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <h1>Predavači</h1>
                </div><!-- end col -->
                {{--<div class="col-md-6 text-right">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Courses</a></li>
                            <li class="active">Course Instructors</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- end col -->--}}
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->

    <section class="white section">
        <div class="container">

            <div class="row course-list teacher-list">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="shop-item-list entry">
                        <div class="">
                            <img src="upload/plesinac.jpg" alt="">
                            <div class="magnifier">
                            </div>
                        </div>
                    </div><!-- end shop-item -->
                </div><!-- end col -->

                <div class="col-md-6 col-md-6">
                    <div class="shop-list-desc">
                        <h4><a href="#">Prof. dr sc. med. Snežana Plešinac</a></h4>
                        <div class="shopmeta">
                            <span class="pull-left"><strong>Profesija : vanredni profesor Medicinskog fakulteta u Beogradu</strong></span>
                            <div class="rating pull-right">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- end rating -->
                        </div><!-- end shop-meta -->

                        <hr class="invis clearfix">

                        <p>Prof. dr sc. med. Snežana Plešinac sa Klinike za ginekologiju i akušerstvo Kliničkog centra
                            Srbije, subspecijalista perinatologije, vanredni profesor Medicinskog fakulteta u Beogradu,
                            predsednica Društva za fetalnu i neonatalnu medicinu Srbije.</p>

                    </div><!-- end shop-list-desc -->
                </div><!-- end col -->

            </div><!-- end row -->

            <div class="row course-list teacher-list">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="shop-item-list entry">
                        <div class="">
                            <img src="upload/sladjana.jpg" alt="">
                            <div class="magnifier">
                            </div>
                        </div>
                    </div><!-- end shop-item -->
                </div><!-- end col -->

                <div class="col-md-6 col-md-6">
                    <div class="shop-list-desc">
                        <h4><a href="#">Viša medicinska sestra Slađana Crvenkov</a></h4>
                        <div class="shopmeta">
                            <span class="pull-left"><strong>Profesija : Viša medicinska sestra u Domu zdravlja Novi Sad</strong></span>
                            <div class="rating pull-right">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- end rating -->
                        </div><!-- end shop-meta -->

                        <hr class="invis clearfix">

                        <p>Viša medicinska sestra Slađana Crvenkov, zaposlena u Domu zdravlja Novi Sad, radi na
                            psihofizičkoj pripremi trudnica za porođaj. Autorka je edicije “Treba znati”, četiri knjige
                            koje se bave temama trudnoće, nege beba, roditeljstva…</p>

                        {{--<a href="course-list.html" class="btn btn-default"><i class="fa fa-search"></i> &nbsp; View My
                            Courses</a>--}} <a href="mailto:sladjanacrvenkov@gmail.com" class="btn btn-primary"><i
                                    class="fa fa-envelope-o"></i> &nbsp; Kontaktirajte me</a>
                    </div><!-- end shop-list-desc -->
                </div><!-- end col -->
            </div><!-- end row -->


            <div class="row course-list teacher-list">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="shop-item-list entry">
                        <div class="">
                            <img src="upload/bojovic.jpg" alt="">
                            <div class="magnifier">
                            </div>
                        </div>
                    </div><!-- end shop-item -->
                </div><!-- end col -->

                <div class="col-md-6 col-md-6">
                    <div class="shop-list-desc">
                        <h4><a href="#">Primarijus dr Ivan Bojović</a></h4>
                        <div class="shopmeta">
                            <span class="pull-left"><strong>Profesija : Specijalista ginekologije i akušerstva</strong></span>
                            <div class="rating pull-right">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- end rating -->
                        </div><!-- end shop-meta -->

                        <hr class="invis clearfix">

                        <p>Konsultant u Bel Medicu, Medigroup, Ordinacija Mladenovic, Poliklinika Labomedika.</p>

                    </div><!-- end shop-list-desc -->
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row course-list teacher-list">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="shop-item-list entry">
                        <div class="">
                            <img src="upload/dusica.jpg" alt="">
                            <div class="magnifier">
                            </div>
                        </div>
                    </div><!-- end shop-item -->
                </div><!-- end col -->

                <div class="col-md-6 col-md-6">
                    <div class="shop-list-desc">
                        <h4><a href="#">Dušica Dišić Kovačević</a></h4>
                        <div class="shopmeta">
                            <span class="pull-left"><strong>Profesija : Viša medicinska sestra-bebica</strong></span>
                            <div class="rating pull-right">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- end rating -->
                        </div><!-- end shop-meta -->

                        <hr class="invis clearfix">

                        <p>Osnivač Fit i zdrava mama programa i prvog postporođajnog servisa Bebi patrola..</p>

                    </div><!-- end shop-list-desc -->
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row course-list teacher-list">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="shop-item-list entry">
                        <div class="">
                            <img src="upload/snezana.jpg" alt="">
                            <div class="magnifier">
                            </div>
                        </div>
                    </div><!-- end shop-item -->
                </div><!-- end col -->

                <div class="col-md-6 col-md-6">
                    <div class="shop-list-desc">
                        <h4><a href="#">Snežana Milanović</a></h4>
                        <div class="shopmeta">
                            <span class="pull-left"><strong>Profesija : Diplomirani medicinar fizioterapeut</strong></span>
                            <div class="rating pull-right">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- end rating -->
                        </div><!-- end shop-meta -->

                        <hr class="invis clearfix">

                        <p>Osnivač privatne prakse  - "Centar za korektivnu gimnastiku SM". Osnivač Grupe za podršku roditeljstvu "Porodično gnezdo". Koautor priručnika "Od zdravih roditelja do zdravog potomstva". Licencirani IAIM instruktor masaže beba.</p>

                    </div><!-- end shop-list-desc -->
                </div><!-- end col -->
            </div><!-- end row -->

        <hr class="invis">

        </div><!-- end container -->
    </section><!-- end section -->

@endsection