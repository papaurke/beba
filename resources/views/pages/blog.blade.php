@extends('index')
@section('content')

    <section class="grey section blogs">
        <div class="container">

            <div class="row">
                <div id="content" class="col-md-12 col-sm-12 col-xs-12">
                    <div class="blog-wrapper">
                        @if(isset($data))
                            <div class="row second-bread">
                                <div class="col-md-6 text-left">
                                    <h1 style="font-style: bold">Blogovi</h1>
                                </div><!-- end col -->
                            </div><!-- end row -->
                        @endif
                    </div><!-- end blog-wrapper -->
                    @if(isset($data))
                        @foreach ($data as $blog)
                            <div class="blog-wrapper one-blog" style="overflow: auto;">
                                <div class="col-md-4 col-sm-12 img-container">
                                    <img src="{{ $blog->image }}"/>
                                </div>
                                <div class="col-md-8 col-sm-12">
                                    <div class="blog-title">
                                        <h2><a href="/blog/{{ $blog->title }}" title="">{{ str_replace('-',' ', $blog->title) }}</a></h2>

                                    </div><!-- end blog-title -->

                                    <div class="blog-desc">
                                        <div class="post-date">
                                            <span class="day">{{ $blog->created_at->format('d') }}</span>
                                            <span class="month">{{ $blog->created_at->format('M') }}</span>
                                        </div>
                                            <p class="content">{!! $blog->content !!}</p>
                                        <a href="/blog/{{ $blog->title }}" class="btn btn-default readmore blog-button">Pročitaj</a>
                                    </div><!-- end desc -->
                                </div>
                            </div><!-- end blog-wrapper -->
                        @endforeach
                    @else
                        <div class="blog-wrapper">
                                <div class="single-img-container">
                                    <img src="{{ $blog->image }}"/>
                                </div>

                                <div class="blog-title single-blog-title">
                                    <h2>{{ str_replace('-',' ', $blog->title) }}</h2>
                                </div><!-- end blog-title -->

                                <div class="social-buttons-container">
                                    <a class="btn btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u=&t=" title="Share on Facebook" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;">
                                        <span class="fa fa-facebook"></span>
                                    </a>

                                    <a class="btn btn-twitter" href="https://twitter.com/intent/tweet?" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=%20Check%20up%20this%20awesome%20content' + encodeURIComponent(document.title) + ':%20 ' + encodeURIComponent(document.URL)); return false;">
                                        <span class="fa fa-twitter"></span>
                                    </a>
                                    <a class="btn btn-gplus" href="https://plus.google.com/share?url=" target="_blank" title="Share on Google+" onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL)); return false;">
                                        <span class="fa fa-google-plus"></span>
                                    </a>
                                    <a class="btn btn-pinterest" title="Pin it" target="_blank" href='javascript:void((function()%7Bvar%20e=document.createElement(&apos;script&apos;); e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;); e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;); e.setAttribute(&apos;src&apos;,&apos; http://assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)%7D)());'>
                                        <span class="fa fa-pinterest"></span>
                                    </a>
                                </div>

                                  <div class="blog-desc single-blog-content">
                                    <div class="post-date">
                                        <span class="day">{{ $blog->created_at->format('d') }}</span>
                                        <span class="month">{{ $blog->created_at->format('M') }}</span>
                                    </div>
                                    <p>{!! $blog->content !!}</p>
                                </div><!-- end desc -->

                        </div><!-- end blog-wrapper -->

                        <section class="grey section other-blogs">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section-title text-center">
                                            <h4>Sa bloga</h4>
                                            <p>Podelićemo sa vama najbolje teme</p>
                                        </div>
                                    </div><!-- end col -->
                                </div><!-- end row -->

                                <div class="row blog-widget">
                                    @foreach ($blogs as $data)
                                        <div class="col-md-4 col-sm-6">
                                            <div class="blog-wrapper">
                                                <div class="blog-title home-blog">
                                                    <a class="category_title" href="#" title=""></a>
                                                    <h2><a class="blogTitle" href="/blog/{{ $blog->title }}" title="">{{ $data->title }}</a></h2>

                                                </div><!-- end blog-title -->
                                                <div class="blog-image">
                                                    <a href="/blog/{{ $blog->title }}" title=""><img src="{{ $data->image }}" alt="" class="img-responsive"></a>
                                                </div><!-- end image -->
                                                <div class="blog-desc">
                                                    <p class="content little-blogs">{!! $data->content !!}</p>
                                                    <a href="/blog/{{ $blog->title }}" class="btn btn-default btn-block">Pročitaj više...</a>
                                                </div><!-- end desc -->
                                            </div><!-- end blog-wrapper -->
                                        </div><!-- end col -->

                                    @endforeach

                                </div><!-- end row -->
                            </div><!-- end container -->
                        </section><!-- end section -->
                    @endif
                </div><!-- end container -->
    </section><!-- end section -->


    <script>
        var el = document.querySelectorAll('.content'),
            titleEl = document.querySelectorAll('.blogTitle'),
            textLength = 300,
            titleLength = 80,
            trimmedTitleEl,
            trimmedText,
            newTitleText,
            text,
            title,
            i;

        // trim function
        function smartTrim(str, length, delim, appendix) {
            if (str.length <= length) return str;

            var trimmedStr = str.substr(0, length + delim.length);

            var lastDelimIndex = trimmedStr.lastIndexOf(delim);
            if (lastDelimIndex >= 0) trimmedStr = trimmedStr.substr(0, lastDelimIndex);

            if (trimmedStr) trimmedStr += appendix;

            return trimmedStr;
        }


        for (i = 0; i < el.length; i++) {
            text = el[i].innerHTML
            trimmedText = smartTrim(text, textLength, ' ', ' ...');

            el[i].innerHTML = trimmedText;
        }

        for (i = 0; i < titleEl.length; i++) {
            title = titleEl[i].innerHTML
            trimmedTitleText = smartTrim(title, titleLength, ' ', ' ...');

            newTitleText = trimmedTitleText.replace(/[-\s]/g," ");
            titleEl[i].innerHTML = newTitleText;
        }
    </script>


@endsection