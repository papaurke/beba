@extends('index')
@section('content')

    <section class="white section premiere">
        <div class="container">
            @if(isset($data))
                <h1>{{ $data->name }}</h1>
                <video oncontextmenu="return false;" width="100%" height="auto" max-height="100%" autoplay>
                    <source src="{{ $data->link }}" type="video/mp4">
                    Vaš browser ne podržava video tag!.
                </video>
                @else
                <p><strong>Dragi roditelji i budući roditelji,</strong><br>

                pilot faza emitovanja Bebinara se završavaju 19.02. kada krećemo sa novom platformom, koja će Vam omogućiti neometan pristup našim sadržajima. <br>

                Vidimo se!<br>
                Bebinar tim</p>

                <p><strong>Četvrtak 01.02.</strong><br>

                18h: Prvih 6 do 8 nedelja sa bebom kod kuće (Babinje) - vms Slađana Crvenkov (oko 35 minuta)</p>
            @endif
        </div><!-- end container -->
    </section><!-- end section -->

@endsection